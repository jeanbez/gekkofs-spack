# Copyright 2013-2021 Lawrence Livermore National Security, LLC and other
# Spack Project Developers. See the top-level COPYRIGHT file for details.
#
# SPDX-License-Identifier: (Apache-2.0 OR MIT)

from spack import *


class Gekkofs(CMakePackage):
    """GekkoFS is a file system capable of aggregating the local I/O capacity and performance of each compute node
in a HPC cluster to produce a high-performance storage space that can be accessed in a distributed manner.
This storage space allows HPC applications and simulations to run in isolation from each other with regards
to I/O, which reduces interferences and improves performance."""

    homepage = "https://storage.bsc.es/gitlab/hpc/gekkofs"
    git      = "https://storage.bsc.es/gitlab/hpc/gekkofs.git"

    maintainers = ['jeanbez']

    version('latest', branch='master', submodules=True)
    version('0.8', commit='b8385c7f3c414e2b330db9734f8db1e33c50c97b', submodules=True)

    patch('date-tz.patch')
    patch('daemon.patch', when='@0.8')

    variant('build_type',
        default='RelWithDebInfo',
        description='CMake build type',
        values=('Debug', 'Release', 'RelWithDebInfo', 'MinSizeRel')
    )

    variant('tests', default=False, description='Build and runs unit tests')
    variant('forwarding', default=False, description='Enables the I/O forwarding mode')
    variant('agios', default=False, description='Enables the AGIOS scheduler for the forwarding mode')

    depends_on('bzip2')
    depends_on('lz4')
    depends_on('zstd')
    depends_on('uuid')
    depends_on('libfabric@1.8.1 fabrics=psm2,sockets,tcp,udp')
    depends_on('bmi')
    depends_on('mercury@2.0.0 +debug +ofi +mpi +sm +shared +boostsys -checksum')
    depends_on('argobots +perf')
    depends_on('margo')
    depends_on('rocksdb@6.11.4 +shared +static +lz4 +snappy +zlib +rtti')
    depends_on('syscall-intercept')
    depends_on('opa-psm2@11.2.185')
    depends_on('date cxxstd=14 +shared +tz tzdb=system')

    depends_on('agios@1.0', when='@0.8 +agios')
    depends_on('agios@latest', when='@latest +agios')

    def cmake_args(self):
        args = []

        if '+tests' in self.spec:
            args.append('-DGKFS_BUILD_TESTS=ON')

        if '+forwarding' in self.spec:
            args.append('-DGKFS_ENABLE_FORWARDING=ON')

        if '+agios' in self.spec:
            args.append('-DGKFS_ENABLE_AGIOS=ON')

        return args
